FROM devfu/ci:2.4-alpine

ENV app /src
ENV BUNDLE_NO_PRUNE true
ENV BUNDLE_PATH /bundle
RUN bundle config jobs $(nproc)

# Copy the bundle config
# COPY .bundle/config /usr/local/bundle/config

RUN mkdir $app
WORKDIR $app
ADD . $app

VOLUME /bundle

CMD bundle console
