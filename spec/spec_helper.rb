# frozen_string_literal: true

libs = %w[
  active_model
  active_support/core_ext
  pry
  rspec
  validation_matcher
]

libs.each { |lib| require lib }

RSpec.configure do |config|
  Kernel.srand config.seed

  config.filter_run :focus
  config.order = :random
  config.run_all_when_everything_filtered = true

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
    expectations.syntax = :should
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
    mocks.syntax = :should
  end
end

class Thing

  extend  ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :field_a, :field_b, :field_c

  validates :field_b, presence: true
  validates :field_c, numericality: { allow_nil: false, only_integer: true }

  validate :custom_validator
  validate :another_custom_validator, on: :update
  validate :procs_cannot_be_speced,   if: -> { new_record? }

private

  def custom_validator
    errors[:base] << 'Field A should be false-y' if field_a
  end

end
