# frozen_string_literal: true

require 'spec_helper'

describe Thing do

  describe 'each validators' do

    describe 'with options' do

      it 'can be expected without specifying options' do
        should validate(:numericality).of :field_c
      end

      it 'can be expected with specified options' do
        should validate(:numericality).of(:field_c).with only_integer: true, allow_nil: false
      end

      it 'cannot be negated with specified options' do
        lambda do
          opts = { only_integer: false }
          should_not validate(:numericality).of(:field_c).with opts
        end.should raise_exception RuntimeError
      end

    end

    describe 'without options' do

      it { should_not validate(:presence).of :field_a }
      it { should     validate(:presence).of :field_b }

    end

  end

  describe 'custom validations' do

    describe 'without options' do

      it { should validate :custom_validator }
      it { should validate :another_custom_validator }

    end

    describe 'with options' do

      it { should validate(:another_custom_validator).with on: :update }

    end

    describe 'with a proc' do

      it 'cannot be specced by key & value' do
        lambda do
          should validate(:procs_cannot_be_speced).with if: -> { new_record? }
        end.should raise_exception RuntimeError
      end

      it 'can be specced by key ONLY' do
        should validate(:procs_cannot_be_speced).with :if
      end

    end

  end

end
