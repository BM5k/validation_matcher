Validation Matcher [![code climate][cc-image]][cc] [![build status][ci-image]][ci] [![gem version][gem-image]][gem]
===================================================================================================================

About
-----

Use Rails validation reflection to test validations.

URLs:

- Github:             https://github.com/bm5k/validation_matcher/
- Documentation:      http://rubydoc.info/github/BM5k/validation_matcher/master/frames
- RubyGems:           https://rubygems.org/gems/validation_matcher

Installation
------------

Add to your Gemfile and run the `bundle` command to install it.

  ```ruby
  gem 'validation_matcher'
  ```

- specify '~> 1.1.0' for rails 4 & rspec 2
- specify '~> 1.0.0' for rails 3 & rspec 2

**Requires Ruby 2.1.0 or later.**

- specify '~> 2' for ruby <2.1

Usage
-----

  ```ruby
  class Foo < ActiveRecord::Base
    validates :field_a, presence: true
    validates :field_b, uniquness: true
    validates :field_c, presence: true, uniquness: {case_insensitive: false}
  end

  require 'spec_helper'

  describe Foo do
    it { should validate(:presence).of(:field_a) }
    it { should validate(:presence).of(:field_b) }
    it { should validate(:presence).of(:field_c).with(case_insensitive: false) }
    it { should validate(:uniqueness).of(:field_b).with(options) }
  end
  ```

<!-- links -->
[cc]: https://codeclimate.com/github/BM5k/validation_matcher "code climate"
[ci]: http://travis-ci.org/BM5k/validation_matcher "build status"
[gem]: http://badge.fury.io/rb/validation_matcher "gem version"

<!-- images -->
[cc-image]: https://codeclimate.com/github/BM5k/validation_matcher.svg
[ci-image]: https://secure.travis-ci.org/BM5k/validation_matcher.svg?branch=master
[gem-image]:https://badge.fury.io/rb/validation_matcher.svg
