# frozen_string_literal: true

require 'validation_matcher/version'
require 'rspec/expectations'

module ValidationMatcher

  RSpec::Matchers.define :validate do |_expected|
    chain(:of) { |attr| @attribute = attr }

    chain(:with) do |*opts|
      @options = opts.extract_options!
      @options = opts if @options.empty?
    end

    match do |_actual|
      if @attribute
        validator.present? && (@options ? validator.options.should =~ @options : true)
      else
        callback.present? && (@options ? check_callback_options : true)
      end
    end

    match_when_negated do |_actual|
      if @attribute
        raise "Negating a matcher with options doesn't really make sense!" if @options
        validator.should be_nil
      else
        callback.should be_nil
      end
    end

    def callback
      actual.class._validate_callbacks.detect { |c| c.filter == expected }
    end

    def check_callback_options
      opts = parse_callback_options

      if @options.is_a? Hash
        raise 'Cannot expect custom procs' if @options.values.any? { |v| v.is_a? Proc }
        @options.each { |k, _| opts[k].should =~ Array(@options[k]) }
      else
        opts.keys.should =~ @options
      end
    end

    def parse_callback_options
      ifs = callback.instance_variable_get '@if'

      if ifs.first.is_a? Proc
        proc_binding = ifs.first.binding
        if proc_binding.local_variables.include? :options
          ons = proc_binding.local_variable_get(:options)[:on]
          ifs = ifs[1..-1]
        end
      end

      {
        if:     Array(ifs),
        on:     Array(ons),
        unless: Array(callback.instance_variable_get('@unless'))
      }.reject { |_, v| v.empty? }
    end

    def validator
      actual.class.validators_on(@attribute).detect { |v| v.kind == expected }
    end
  end

end
